USE AXITY
/*CREAMOS TABLA EMPLEADO*/

CREATE TABLE dbo.empleados (
id INT IDENTITY,
nombre VARCHAR(50),
edad INT,
fechaNacimiento DATETIME

CONSTRAINT PK_empleado PRIMARY KEY (id)
)

/*INSERTAMOS 3 REGISTROS PARA QUE NOS SIRVAN DE PRUEBAS*/

INSERT INTO dbo.empleados (nombre, edad,fechaNacimiento) VALUES ('Rafael Gonz�lez Gonz�lez', 31, '1989-03-03')
INSERT INTO dbo.empleados (nombre, edad,fechaNacimiento) VALUES ('Jorge Santos Santos', 30, '1990-03-03')
INSERT INTO dbo.empleados (nombre, edad,fechaNacimiento) VALUES ('Roberto Suarez Santos', 29, '1991-03-03')
GO

/*CREAMOS PROCEDIMIENTOS ALMACENADOS */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Autor:				Rafael Gonz�lez
-- Fecha de creaci�n:	2020-11-18
-- Descripci�n:			Muestra el listado de la tabla empleados
-- =============================================
CREATE PROCEDURE [dbo].[spempleado_consultar]
AS
BEGIN
	
	SET NOCOUNT ON
    SELECT id, nombre, edad, fechaNacimiento FROM empleados
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Autor:				Rafael Gonz�lez
-- Fecha de creaci�n:	2020-11-18
-- Descripci�n:			Elimina un registro por id de la tabla empleados
-- =============================================
CREATE PROCEDURE [dbo].[spempleado_eliminar]
	@id INT
AS
BEGIN
	
	SET NOCOUNT ON
    DECLARE @Mensaje VARCHAR(MAX) = ''
	DECLARE @Satisfactorio BIT = 1
	DECLARE @ErrorId INT = 0

	BEGIN TRY
		DELETE FROM dbo.empleados WHERE id = @id
	END TRY
	BEGIN CATCH
		SET @ErrorId = ERROR_NUMBER()
		SET @Satisfactorio = 0
		SET @Mensaje = ERROR_MESSAGE()		
	END CATCH
	
	
	SELECT @ErrorId 'ErrorId', @Satisfactorio 'Satisfactorio', @Mensaje 'Mensaje'
END
GO

